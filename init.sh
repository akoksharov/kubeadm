#!/bin/sh

set -ex

IPAD=$(ip ad show dev ens160 | awk '/\s+inet\s/ {print $2}')
IP_ADDRESS=${IPAD%\/*}
API_FQDN=app.int.eventito.com

apt-get update && apt-get install -y apt-transport-https

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee > /etc/apt/sources.list.d/kubernetes.list

apt-get update && apt-get install -y docker.io kubeadm

cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

swapoff -a
sudo sed -i.bak '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

systemctl enable docker.service
systemctl restart docker.service

sudo kubeadm config images pull

sudo kubeadm init \
    --apiserver-cert-extra-sans=${IP_ADDRESS},${API_FQDN} \
    --node-name=$(hostname) \
    --pod-network-cidr=192.168.0.0/16

# configure kubectl
mkdir -p ~/.kube
sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
sudo chown $(id -u):$(id -g) ~/.kube/config

# allow pods to run on this master node
kubectl taint nodes --all node-role.kubernetes.io/master-

# install network plugin
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
